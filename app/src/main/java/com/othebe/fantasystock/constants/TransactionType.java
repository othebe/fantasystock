package com.othebe.fantasystock.constants;

/**
 * Created by otheb on 3/14/2016.
 */
public enum TransactionType {
  BUY("BUY"),
  SELL("SELL"),
  GIFT("GIFT");

  private final String __text;

  TransactionType(final String text) {
    this.__text = text;
  }

  @Override
  public String toString() {
    return this.__text;
  }
}
