package com.othebe.fantasystock.constants;

/**
 * Created by otheb on 3/12/2016.
 */
public enum StockType {
  STOCK,
  EQUITY,
  OTHER
}
