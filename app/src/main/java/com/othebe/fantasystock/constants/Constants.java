package com.othebe.fantasystock.constants;

/**
 * Created by otheb on 3/13/2016.
 */
public class Constants {
  public static final String DB_NAME = "fantasystock.db";
  public static final int DB_VERSION = 1;

  public static final float TRANSACTION_COST = 7.95f;
  public static final String CASH_RESERVE_SYMBOL = "--cash--";
  public static final int PORTFOLIO_STARTING_MONEY = 25000;
}
