package com.othebe.fantasystock.common;

/**
 * Created by otheb on 4/20/2016.
 */
public class ValidationResult {
  private boolean __success;
  private String __details;

  public ValidationResult(boolean success, String details) {
    this.__success = success;
    this.__details = details;
  }

  public boolean isSuccess() {
    return this.__success;
  }

  public String getDetails() {
    return this.__details;
  }
}
