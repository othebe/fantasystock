package com.othebe.fantasystock.ticker;

import android.util.Log;

import com.othebe.fantasystock.models.Stock;

import java.util.ArrayList;
import java.util.Stack;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import rx.Notification;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func2;
import rx.observables.ConnectableObservable;
import rx.schedulers.Schedulers;

/**
 * Created by otheb on 4/9/2016.
 */
public class Ticker {
  private static final long REFRESH_INTERVAL = 1L;
  private static final TimeUnit REFRESH_INTERVAL_TIME_UNIT = TimeUnit.MINUTES;

  private Stock __stock;

  private Observable __tickerObservable;
  private ConnectableObservable __tickerConnectableObservable;
  private Subscription __tickerSubscription;
  private Stack<Boolean> __tickerSubscribers;

  public Ticker(String symbol) {
    this.__stock = new Stock(symbol);
    this.__setupObservables();
  }

  public Ticker(Stock stock) {
    this.__stock = stock;
    this.__setupObservables();
  }

  /**
   * Setup observable stuff.
   */
  private void __setupObservables() {
    final Ticker self = this;
    this.__tickerSubscribers = new Stack<>();

    Observable fetcher = Observable.fromCallable(new Callable() {
      @Override
      public Object call() throws Exception {
        // TODO: We should make it so that the observable returns a stock.
        self.__stock.refresh();
        return null;
      }
    });

    this.__tickerConnectableObservable = fetcher
      .mergeWith(Observable.interval(REFRESH_INTERVAL, REFRESH_INTERVAL_TIME_UNIT))
      .subscribeOn(Schedulers.io())
      .doOnEach(new Action1<Notification>() {
        @Override
        public void call(Notification notification) {
          Log.d("TICKER", notification.toString());
        }
      }).replay(1);
    this.startTick();
  }

  /**
   * Start ticking.
   */
  public void startTick() {
    if (this.__tickerSubscription == null || this.__tickerSubscription.isUnsubscribed()) {
       this.__tickerSubscription = this.__tickerConnectableObservable.connect();
    }
  }

  /**
   * Stop ticking.
   */
  public void stopTick() {
    if (this.__tickerSubscription != null && !this.__tickerSubscription.isUnsubscribed()) {
      Log.d("TICKER", "Stopping ticker - " + this.__stock.getSymbol());
      this.__tickerSubscription.unsubscribe();
    }
  }

  public Observable<Void> getObservable() {
    if (this.__tickerObservable != null) {
      return this.__tickerObservable;
    }

    final Ticker self = this;

    // Handle subscriptions.
    Action0 onSubscribe = new Action0() {
      @Override
      public void call() {
        self.__tickerSubscribers.push(true);
        if (self.__tickerSubscribers.size() > 0) {
          self.startTick();
        }
      }
    };

    // Handle unsubscriptions.
    Action0 onUnsubscribe = new Action0() {
      @Override
      public void call() {
        self.__tickerSubscribers.pop();
        if (self.__tickerSubscribers.size() == 0) {
          self.stopTick();
        }
      }
    };

    this.__tickerObservable = this.__tickerConnectableObservable
      .doOnSubscribe(onSubscribe)
      .doOnUnsubscribe(onUnsubscribe);
    return this.__tickerObservable;
  }
}
