package com.othebe.fantasystock;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.othebe.fantasystock.daos.Daos;
import com.othebe.fantasystock.ui.SummaryActivity;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    this.__initializeDatabase();

    Intent intent = new Intent(this, SummaryActivity.class);
    startActivity(intent);
  }

  private void __initializeDatabase() {
    Daos daos = new Daos(this);
    daos.initializeTables();
  }
}

// TODO: Add loading state animations.
