package com.othebe.fantasystock;

import android.app.SearchManager;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.othebe.fantasystock.R;
import com.othebe.fantasystock.ui.StockDetailsActivity;

public class SearchResultsActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_search_results);

    this.__handleIntent(getIntent());
  }

  @Override
  protected void onNewIntent(Intent intent) {
    super.onNewIntent(intent);

    setIntent(intent);
    this.__handleIntent(intent);
  }

  private void __handleIntent(Intent intent) {
    String a = Intent.ACTION_VIEW;
    String b = intent.getAction();
    boolean c = a.equals(b);
    int d = a.compareTo(b);

    if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
      String query = intent.getStringExtra(SearchManager.QUERY);
      Log.i("INFO", query);
    }
    else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
      Bundle bundle = new Bundle();
      bundle.putString(StockDetailsActivity.BUNDLE_SYMBOL, intent.getDataString());
      Intent stockDetails = new Intent(this, StockDetailsActivity.class);
      stockDetails.putExtras(bundle);
      startActivity(stockDetails);
    }
  }
}
