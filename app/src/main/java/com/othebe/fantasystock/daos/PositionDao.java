package com.othebe.fantasystock.daos;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.othebe.fantasystock.models.Position;

/**
 * Created by otheb on 3/14/2016.
 */
public class PositionDao extends BaseDao {
  private static final String TABLE_NAME = "positions";

  private static final String COL_SYMBOL = "symbol";
  private static final String COL_UNITS = "units";
  private static final String COL_TOTAL_COST = "total_cost";

  public PositionDao(SQLiteOpenHelper sqLiteOpenHelper) {
    super(sqLiteOpenHelper);
  }

  @Override
  protected void createTable(SQLiteDatabase db) {
    String q = "CREATE TABLE " + TABLE_NAME + "(" +
      COL_SYMBOL + " CHAR(50) NOT NULL," +
      COL_UNITS + " REAL NOT NULL," +
      COL_TOTAL_COST + " REAL NOT NULL" +
      ")";
    db.execSQL(q);
  }

  /**
   * Update database for position.
   * @param position
   */
  public void updatePosition(final Position position) {
    ContentValues contentValues = new ContentValues();
    contentValues.put(COL_SYMBOL, position.getSymbol());
    contentValues.put(COL_UNITS, position.getUnits());
    contentValues.put(COL_TOTAL_COST, position.getTotalCost());

    Position existing = findBySymbol(position.getSymbol());
    SQLiteDatabase db = getWritableDatabase();

    if (existing == null) {
      db.insert(TABLE_NAME, null, contentValues);
    } else {
      db.update(
        TABLE_NAME,
        contentValues,
        COL_SYMBOL + " =?",
        new String[] { position.getSymbol()});
    }
    db.close();
  }

  /**
   * Find a position by symbol.
   * @param symbol
   * @return
   */
  public Position findBySymbol(String symbol) {
    SQLiteDatabase db = getReadableDatabase();

    Cursor existing = db.query(
      TABLE_NAME,
      new String[] { COL_UNITS, COL_TOTAL_COST },
      COL_SYMBOL + " =?",
      new String[] { symbol },
      null, null, null);

    Position position = null;
    if (existing.getCount() > 0) {
      existing.moveToFirst();
      float units = existing.getFloat(0);
      float totalCost = existing.getFloat(1);
      position = new Position(symbol, units, totalCost);
    }

    db.close();
    return position;
  }

  public Position[] getAllPositions() {
    SQLiteDatabase db = getReadableDatabase();
    Cursor c = db.query(
      TABLE_NAME,
      new String[] { COL_SYMBOL, COL_UNITS, COL_TOTAL_COST },
      COL_UNITS + ">0",
      null, null, null, null);

    c.moveToFirst();
    int numPositions = c.getCount();
    Position[] positions = new Position[numPositions];
    for (int i = 0; i < numPositions; i++) {
      String symbol = c.getString(0);
      float units = c.getFloat(1);
      float totalCost = c.getFloat(2);
      positions[i] = new Position(symbol, units, totalCost);
      c.moveToNext();
    }

    db.close();
    return positions;
  }

//  public Position[] getAllPositions() {
//    this.open();
//    // Get all positions.
//    String[] rows = { COL_SYMBOL, COL_UNITS, COL_TOTAL_COST };
//    Cursor c = this.__db.query(
//      TABLE_NAME,
//      rows,
//      null,
//      null,
//      null,
//      null,
//      null
//    );
//
//    int numPositions = c.getCount();
//    Position[] positions = new Position[numPositions];
//    for (int i = 0; i < numPositions; i++) {
//      Position p = new Position();
//      p.setSymbol(c.getString(0));
//      p.setUnits(c.getFloat(1));
//      p.setTotalCost(c.getFloat(2));
//
//      positions[i] = p;
//    }
//
//    this.close();
//    return positions;
//  }
//
//  public void resetPositions() {
//    this.open();
//    this.__db.delete(
//      TABLE_NAME,
//      null,
//      null
//    );
//    this.__addCash();
//    this.close();
//  }
//
//  private void __addCash() {
//    String q = "INSERT INTO " + TABLE_NAME + "(" +
//      COL_SYMBOL + "," +
//      COL_UNITS + "," +
//      COL_TOTAL_COST +
//      ") VALUES (" +
//      Constants.CASH_RESERVE_SYMBOL + "," +
//      Constants.PORTFOLIO_STARTING_MONEY + "," +
//      Constants.PORTFOLIO_STARTING_MONEY +
//      ")";
//    this.__db.execSQL(q);
//  }
}
