package com.othebe.fantasystock.daos;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by otheb on 4/11/2016.
 */
public abstract class BaseDao {
  protected SQLiteOpenHelper sqLiteOpenHelper;

  protected abstract void createTable(SQLiteDatabase db);

  public BaseDao(SQLiteOpenHelper sqLiteOpenHelper) {
    this.sqLiteOpenHelper = sqLiteOpenHelper;
  }

  public SQLiteDatabase getReadableDatabase() {
    return this.sqLiteOpenHelper.getReadableDatabase();
  }

  public SQLiteDatabase getWritableDatabase() {
    return this.sqLiteOpenHelper.getWritableDatabase();
  }
}
