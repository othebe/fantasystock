package com.othebe.fantasystock.daos;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.othebe.fantasystock.constants.TransactionType;
import com.othebe.fantasystock.models.Stock;

import java.util.Date;

import rx.functions.Action1;

/**
 * Created by otheb on 3/14/2016.
 */
public class TransactionDao extends BaseDao {
  private static final String TABLE_NAME = "transactions";

  private static final String COL_SYMBOL = "symbol";
  private static final String COL_TRANSACTION_TYPE = "transaction_type";
  private static final String COL_UNITS = "units";
  private static final String COL_PRICE_PER_UNIT = "price_per_unit";
  private static final String COL_TRANSACTION_EPOCH = "transaction_epoch";

  public TransactionDao(SQLiteOpenHelper sqLiteOpenHelper) {
    super(sqLiteOpenHelper);
  }

  @Override
  protected void createTable(SQLiteDatabase db) {
    String q = "CREATE TABLE " + TABLE_NAME + "(" +
      COL_SYMBOL + " CHAR(50) NOT NULL," +
      COL_TRANSACTION_TYPE + " CHAR(50) NOT NULL," +
      COL_UNITS + " REAL NOT NULL," +
      COL_PRICE_PER_UNIT + " REAL NOT NULL," +
      COL_TRANSACTION_EPOCH + " INTEGER NOT NULL" +
      ")";
    db.execSQL(q);
  }

  public void createTransaction(Stock stock,
                                TransactionType transactionType,
                                double pricePerUnit,
                                double units) {
    ContentValues contentValues = new ContentValues();
    contentValues.put(COL_SYMBOL, stock.getSymbol());
    contentValues.put(COL_TRANSACTION_TYPE, transactionType.toString());
    contentValues.put(COL_PRICE_PER_UNIT, pricePerUnit);
    contentValues.put(COL_UNITS, units);
    contentValues.put(COL_TRANSACTION_EPOCH, new Date().getTime());

    SQLiteDatabase db = getWritableDatabase();
    db.insert(TABLE_NAME, null, contentValues);
    db.close();
  }

  /**
   * Add starting cash.
   * @param db
   * @param value
   */
  public void addStartingCash(SQLiteDatabase db, double value) {
    ContentValues contentValues = new ContentValues();
    contentValues.put(COL_SYMBOL, "");
    contentValues.put(COL_TRANSACTION_TYPE, TransactionType.GIFT.toString());
    contentValues.put(COL_PRICE_PER_UNIT, 1.00);
    contentValues.put(COL_UNITS, value);
    contentValues.put(COL_TRANSACTION_EPOCH, new Date().getTime());

    db.insert(TABLE_NAME, null, contentValues);
  }
}
