package com.othebe.fantasystock.daos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by otheb on 3/13/2016.
 */
public class UserDao extends BaseDao {
  public static final String TABLE_NAME = "users";

  public static final String COL_USERNAME = "username";
  public static final String COL_CASH = "cash";

  private static final String DEFAULT_USERNAME = "Unregistered";

  public UserDao(SQLiteOpenHelper sqLiteOpenHelper) {
    super(sqLiteOpenHelper);
  }

  @Override
  protected void createTable(SQLiteDatabase db) {
    String q = "CREATE TABLE " + TABLE_NAME + "(" +
      COL_USERNAME + " CHAR(128) DEFAULT NULL," +
      COL_CASH + " REAL NOT NULL" +
      ")";
    db.execSQL(q);
    this.__addDefaultUser(db);
  }

  /**
   * Add default user.
   * @param db
   */
  private void __addDefaultUser(SQLiteDatabase db) {
    String q = "INSERT INTO " + TABLE_NAME + "(" +
      COL_USERNAME +
      ") VALUES(" +
      DEFAULT_USERNAME +
      ")";
    db.execSQL(q);
  }

  /**
   * Update starting cash.
   * @param cash
   */
  public void updateStartingCash(SQLiteDatabase db, float cash) {
    ContentValues contentValues = new ContentValues();
    contentValues.put(COL_CASH, cash);

    db.insert(TABLE_NAME, null, contentValues);
  }

  public double getAvailableCash() {
    double availableCash = 0;
    SQLiteDatabase db = getReadableDatabase();
    Cursor c = db.query(
      TABLE_NAME,
      new String[] { COL_CASH },
      null, null, null, null, null);

    c.moveToFirst();
    availableCash = c.getDouble(0);
    db.close();

    return availableCash;
  }

  public void addRemoveCash(double delta) {
    double availableCash = this.getAvailableCash() + delta;
    ContentValues contentValues = new ContentValues();
    contentValues.put(COL_CASH, availableCash);

    SQLiteDatabase db = getWritableDatabase();
    db.update(TABLE_NAME, contentValues, null, null);
    db.close();
  }
}
