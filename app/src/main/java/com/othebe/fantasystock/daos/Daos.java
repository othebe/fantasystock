package com.othebe.fantasystock.daos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.othebe.fantasystock.constants.Constants;
import com.othebe.fantasystock.models.Transaction;

import java.lang.reflect.Constructor;

/**
 * Created by otheb on 4/10/2016.
 */
public class Daos extends SQLiteOpenHelper {
  private static final String LOG_TAG = "Daos";
  private static final Object[] registeredDaos = {
    PositionDao.class,
    TransactionDao.class,
    UserDao.class
  };

  protected Context context;

  private static PositionDao __positionDao;
  private static TransactionDao __transactionDao;
  private static UserDao __userDao;

  public Daos(Context context) {
    super(context, Constants.DB_NAME, null, Constants.DB_VERSION);
    this.context = context;
  }

  public void initializeTables() {
    SQLiteDatabase db = getWritableDatabase();
    db.close();
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    // Create tables.
    for (Object o : registeredDaos) {
      try {
        Constructor<BaseDao> ctor = ((Class<BaseDao>) o).getDeclaredConstructor(SQLiteOpenHelper.class);
        ctor.newInstance(this).createTable(db);
      } catch (Exception e) {
        Log.e(LOG_TAG, e.toString());
      }
    }

    // Set initial user amount.
    new TransactionDao(this).addStartingCash(db, Constants.PORTFOLIO_STARTING_MONEY);
    new UserDao(this).updateStartingCash(db, Constants.PORTFOLIO_STARTING_MONEY);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) { }

  /**
   * Open writable database.
   * @param context
   * @return
   */
  public static SQLiteDatabase getWritableDatabase(Context context) {
    Daos daos = new Daos(context);
    return daos.getWritableDatabase();
  }

  /**
   * Close database.
   * @param db
   */
  public static void close(SQLiteDatabase db) {
    db.close();
  }

  /**
   * Get Position DAO.
   * @param context
   * @return
   */
  public static PositionDao getPositionDao(Context context) {
    if (__positionDao == null) {
      __positionDao = new PositionDao(new Daos(context));
    }
    return __positionDao;
  }

  /**
   * Get Transaction DAO.
   * @param context
   * @return
   */
  public static TransactionDao getTransactionDao(Context context) {
    if (__transactionDao == null) {
      __transactionDao = new TransactionDao(new Daos(context));
    }
    return __transactionDao;
  }

  /**
   * Get User DAO.
   * @param context
   * @return
   */
  public static UserDao getUserDao(Context context) {
    if (__userDao == null) {
      __userDao = new UserDao(new Daos(context));
    }
    return __userDao;
  }
}
