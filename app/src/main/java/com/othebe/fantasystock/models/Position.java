package com.othebe.fantasystock.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.othebe.fantasystock.constants.Constants;
import com.othebe.fantasystock.constants.TransactionType;
import com.othebe.fantasystock.daos.Daos;
import com.othebe.fantasystock.daos.PositionDao;

/**
 * Created by otheb on 3/14/2016.
 */
public class Position {
  private String __symbol;
  private float __units;
  private float __totalCost;

  public Position(String symbol, float units, float totalCost) {
    this.__symbol = symbol;
    this.__units = units;
    this.__totalCost = totalCost;
  }

  public String getSymbol() { return this.__symbol; }
  public void setSymbol(String symbol) { this.__symbol = symbol; }
  public float getUnits() { return this.__units; }
  public void setUnits(float units) { this.__units = units; }
  public float getTotalCost() { return this.__totalCost; }
  public void setTotalCost(float totalCost) { this.__totalCost = totalCost; }

  public static Position findBySymbol(Context context, String symbol) {
    PositionDao dao = Daos.getPositionDao(context);
    return dao.findBySymbol(symbol);
  }

  /**
   * Updates the data for this position according to the transaction type.
   * @param context
   * @param stock
   * @param units
   * @param transactionType
   */
  public void updateByTransactionType(Context context,
                                      Stock stock,
                                      double units,
                                      TransactionType transactionType) {
    if (transactionType == TransactionType.BUY) {
      this.__units += units;
      this.__totalCost += (units * stock.getLastTradePrice() + Constants.TRANSACTION_COST);
    }
    else if (transactionType == TransactionType.SELL) {
      this.__units -= units;
      this.__totalCost -= (units * stock.getLastTradePrice() + Constants.TRANSACTION_COST);
    }
    PositionDao positionDao = Daos.getPositionDao(context);
    positionDao.updatePosition(this);
  }

  /**
   * Get all positions the user owns.
   * @param context
   * @return
   */
  public static Position[] getAllPositions(Context context) {
    PositionDao dao = Daos.getPositionDao(context);
    return dao.getAllPositions();
  }
}
