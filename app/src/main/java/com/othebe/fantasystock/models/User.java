package com.othebe.fantasystock.models;

import android.content.Context;

import com.othebe.fantasystock.daos.Daos;

/**
 * Created by otheb on 3/13/2016.
 */
public class User {
  private int __id;
  private String __username;

  public static User getCurrent() {
    return new User();
  }

  public int getId() {
    return this.__id;
  }

  public void setId(int id) {
    this.__id = id;
  }

  public String getUsername() {
    return this.__username;
  }

  public void setUsername(String username) {
    this.__username = username;
  }

  public double getAvailableCash(Context context) {
    return Daos.getUserDao(context).getAvailableCash();
  }

  public void addRemoveCash(Context context, double delta) {
    Daos.getUserDao(context).addRemoveCash(delta);
  }
}
