package com.othebe.fantasystock.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.othebe.fantasystock.stockdetails.StockDetailsFetcher;
import com.othebe.fantasystock.ticker.Ticker;

import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Func0;
import rx.observables.ConnectableObservable;
import rx.schedulers.Schedulers;

/**
 * Created by otheb on 3/12/2016.
 */
public class Stock implements Parcelable {
  public static final String PARCELABLE_KEY = "stock-parcelable-key";

  private String __symbol = "";
  private String __name = "";
  private double __askPrice = -1;
  private double __bidPrice = -1;
  private String __dayValueChange = "";
  private double __dayLow = 0;
  private String __dayHigh = "";
  private String __52WeekLow = "";
  private String __52WeekHigh = "";
  private double __openPrice = 0;
  private double __previousClosingPrice = 0;
  private double __change = 0;
  private String __changePercent = "";
  private String __lastTradeDate = "";
  private String __lastTradeTime = "";
  private double __lastTradePrice = 0;

  private Ticker __ticker;

  /**
   * Create stock from symbol asynchronously.
   * @param symbol
   */
  public Stock(String symbol) {
    this.__symbol = symbol;
  }

  /**
   * Create stock from fetch results.
   * @param fetchResults
   */
  public Stock(HashMap<String, String> fetchResults) {
    this.__parseStockDetails(fetchResults);
  }

  /**
   * Create stock from parcel.
   * @param source
   */
  public Stock(Parcel source) {
    this.__symbol = source.readString();
    this.__askPrice = source.readDouble();
    this.__change = source.readDouble();
    this.__changePercent = source.readString();
    this.__lastTradeDate = source.readString();
    this.__lastTradeTime = source.readString();
  }

  public String getSymbol() { return this.__symbol; }
  public String getName() { return this.__name; }
  public double getAskPrice() { return this.__askPrice; }
  public double getBidPrice() { return this.__bidPrice; }
  public String getDayValueChange() { return this.__dayValueChange; }
  public double getDayLow() { return this.__dayLow; }
  public String getDayHigh() { return this.__dayHigh; }
  public String get52WeekLow() { return this.__52WeekLow; }
  public String get52WeekHigh() { return this.__52WeekHigh; }
  public double getOpeningPrice() { return this.__openPrice; }
  public double getClosingPrice() { return this.__previousClosingPrice; }
  public double getChange() { return this.__change; }
  public String getChangePercent() { return this.__changePercent; }
  public String getLastTradeDate() { return this.__lastTradeDate; }
  public String getLastTradeTime() { return this.__lastTradeTime; }
  public double getLastTradePrice() { return this.__lastTradePrice; }

  /**
   * Create array of stocks from an array of symbols. The created stocks fetch data from
   * the network.
   * @param symbols
   * @return
   */
  public static Stock[] createStocksFromSymbols(final String[] symbols) {
    HashMap<String, String>[] detailsArr = StockDetailsFetcher.fetchStockDetails(symbols, StockDetailsFetcher.ALL_FETCH_OPTIONS);
    Stock[] stocks = new Stock[detailsArr.length];
    for (int i = 0; i < detailsArr.length; i++) {
      stocks[i] = new Stock(detailsArr[i]);
    }

    return stocks;
  }

  /**
   * Refresh stock details. This should be called when creating stocks from symbols.
   * @return Observable that returns stock status on subscription.
   */
  public Observable<Stock> refresh() {
    final Stock self = this;

    // Fetch and refresh stock details.
    final Callable<Stock> refreshStock = new Callable<Stock>() {
      @Override
      public Stock call() throws Exception {
        HashMap<String, String>[] detailsArr = StockDetailsFetcher.fetchStockDetails(
          new String[] { self.__symbol}, StockDetailsFetcher.ALL_FETCH_OPTIONS);
        if (detailsArr.length == 1) {
          HashMap<String, String> details = detailsArr[0];
          self.__parseStockDetails(details);
        }
        return self;
      }
    };

    return Observable
      .fromCallable(refreshStock);
  }

  public Ticker getTicker() {
    if (this.__ticker == null) {
      this.__ticker = new Ticker(this);
    }
    return this.__ticker;
  }

  private void __parseStockDetails(HashMap<String, String> details) {
    this.__symbol = details.get("s");
    this.__name = details.get("n");
    this.__askPrice = __safeParseDouble(details.get("a"));
    this.__bidPrice = __safeParseDouble(details.get("b"));
    this.__dayValueChange = details.get("w1");
    this.__dayLow = __safeParseDouble(details.get("g"));
    this.__dayHigh = details.get("h");
    this.__52WeekLow = details.get("j");
    this.__52WeekHigh = details.get("k");
    this.__openPrice = __safeParseDouble(details.get("o"));
    this.__previousClosingPrice = __safeParseDouble(details.get("p"));
    this.__change = __safeParseDouble(details.get("c1"));
    this.__changePercent = details.get("p2");
    this.__lastTradeDate = details.get("d1");
    this.__lastTradeTime = details.get("t1");
    this.__lastTradePrice = __safeParseDouble(details.get("l1"));
  }

  /**
   * Safely parse value to double.
   * @param value
   * @return
   */
  private static double __safeParseDouble(String value) {
    double parsed = 0;
    try {
      parsed = Double.parseDouble(value);
    } catch (Exception e) {
      Log.e("STOCK", e.toString());
    }

    return parsed;
  }

  /******************************************
   * This section is required for Parcelable.
   *****************************************/

  @Override
  public int describeContents() {
    return 0;
  }

  public static final Parcelable.Creator<Stock> CREATOR = new Parcelable.Creator<Stock>() {
    @Override
    public Stock createFromParcel(Parcel source) {
      return new Stock(source);
    }

    @Override
    public Stock[] newArray(int size) {
      return new Stock[size];
    }
  };

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.__symbol);
    dest.writeDouble(this.getAskPrice());
    dest.writeDouble(this.getChange());
    dest.writeString(this.getChangePercent());
    dest.writeString(this.getLastTradeDate());
    dest.writeString(this.getLastTradeTime());
  }
}
