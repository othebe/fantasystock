package com.othebe.fantasystock.models;

import android.content.Context;

import com.othebe.fantasystock.stockdetails.StockDetailsFetcher;

import java.util.HashMap;

/**
 * Created by otheb on 4/19/2016.
 */
public class Portfolio {
  private PortfolioItem[] __portfolio;

  public Portfolio(PortfolioItem[] portfolio) {
    this.__portfolio = portfolio;
  }

  /**
   * Build a portfolio from current positions.
   * @param context
   * @return
   */
  public static Portfolio buildPortfolio(Context context) {
    Position[] positions = Position.getAllPositions(context);

    String[] symbols = new String[positions.length];
    for (int i = 0; i < positions.length; i++) {
      symbols[i] = positions[i].getSymbol();
    }
    Stock[] stocks = Stock.createStocksFromSymbols(symbols);

    PortfolioItem[] portfolio = new PortfolioItem[positions.length];
    for (int i = 0; i < positions.length; i++) {
      portfolio[i] = new PortfolioItem(stocks[i], positions[i]);
    }

    return new Portfolio(portfolio);
  }

  public double getInvestmentPurchaseAmount() {
    double result = 0;
    for (PortfolioItem p : this.__portfolio) {
      result += p.getTotalCost();
    }

    return result;
  }

  public double getCurrentValuation() {
    double result = 0;
    for (PortfolioItem p : this.__portfolio) {
      result += p.getLastPrice() * p.getQuantity();
    }

    return result;
  }

  public double getTodaysValueChange() {
    double result = 0;
    for (PortfolioItem p: this.__portfolio) {
      result += p.getValueChange();
    }

    return result;
  }

  public double getTodaysPercentChange() {
    double todaysValueChange = this.getTodaysValueChange();
    double previousValuation = this.getCurrentValuation() - this.getTodaysValueChange();

    if (previousValuation == 0) {
      return 0;
    }

    return todaysValueChange / previousValuation * 100;
  }

  public double getOverallValueChange() {
    double result = 0;
    for (PortfolioItem p : this.__portfolio) {
      result += p.getOverallChange();
    }

    return result;
  }

  public double getOverallPercentChange() {
    if (this.getInvestmentPurchaseAmount() == 0) {
      return 0;
    }
    return this.getOverallValueChange() / this.getInvestmentPurchaseAmount() * 100;
  }
}
