package com.othebe.fantasystock.models;

import com.othebe.fantasystock.models.Position;
import com.othebe.fantasystock.models.Stock;

/**
 * Created by otheb on 4/12/2016.
 */
public class PortfolioItem {
  private Stock __stock;
  private Position __position;

  public PortfolioItem(Stock stock, Position position) {
    this.__stock = stock;
    this.__position = position;
  }

  public String getSymbol() {
    return this.__stock.getSymbol();
  }

  public String getName() {
    return this.__stock.getName();
  }

  public double getTotalCost() { return  this.__position.getTotalCost(); }

  public float getQuantity() {
    return this.__position.getUnits();
  }

  public double getLastPrice() {
    return this.__stock.getLastTradePrice();
  }

  /**
   * Current value of this stock position.
   * @return
   */
  public double getCurrentValue() {
    return this.getQuantity() * this.getLastPrice();
  }

  /**
   * Today's change in stock value.
   * @return
   */
  public double getValueChange() {
    return this.getQuantity() * this.__stock.getChange();
  }

  /**
   * Today's percentage change in stock value.
   * @return
   */
  public double getPercentChange() {
    return this.__stock.getChange() / this.__stock.getClosingPrice() * 100;
  }

  /**
   * Overall change in stock value.
   * @return
   */
  public double getOverallChange() {
    return this.getCurrentValue() - this.__position.getTotalCost();
  }

  /**
   * Overall percentage change in stock value.
   * @return
   */
  public double getOverallPercentChange() {
    return this.getOverallChange() / this.__position.getTotalCost() * 100;
  }

  public double getCostBasisPerUnit() {
    return this.__position.getTotalCost() / this.__position.getUnits();
  }
}
