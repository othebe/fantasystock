package com.othebe.fantasystock.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.othebe.fantasystock.constants.TransactionType;
import com.othebe.fantasystock.daos.Daos;
import com.othebe.fantasystock.daos.TransactionDao;

/**
 * Created by otheb on 3/14/2016.
 */
public class Transaction {
  private String __symbol;
  private TransactionType __transactionType;
  private float __units;
  private float __pricePerUnit;
  private long __transactionEpochTime;

  public String getSymbol() { return this.__symbol; }
  public void setSymbol(String symbol) { this.__symbol = symbol; }
  public TransactionType getTransactionType() { return this.__transactionType; }
  public void setTransactionType(TransactionType t) { this.__transactionType = t; }
  public float getUnits() { return this.__units; }
  public void setUnits(float units) { this.__units = units; }
  public float getPricePerUnit() { return this.__pricePerUnit; }
  public void setPricePerUnit(float price) { this.__pricePerUnit = price; }
  public long getTransactionEpochTime() { return this.__transactionEpochTime; }
  public void setTransactionEpochTime(long t) { this.__transactionEpochTime = t; }

  public static void createTransaction(Context context,
                                       Stock stock,
                                       TransactionType transactionType,
                                       double units) {
    double pricePerUnit = stock.getLastTradePrice();
    TransactionDao dao = Daos.getTransactionDao(context);
    dao.createTransaction(stock, transactionType, pricePerUnit, units);
  }
}
