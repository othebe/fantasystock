package com.othebe.fantasystock.stockdetails;

import android.os.AsyncTask;

import com.opencsv.CSVIterator;
import com.opencsv.CSVParser;
import com.opencsv.CSVReader;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import rx.Observable;
import rx.functions.Func0;

/**
 * Created by otheb on 3/12/2016.
 */
public class StockDetailsFetcher {
  public static final String[] ALL_FETCH_OPTIONS = {
    "s",    // Symbol
    "n",    // Name
    "a",    // Ask price
    "b",    // Bid price
    "w1",   // Days value change
    "g",    // Days low
    "h",    // Days high
    "j",    // 52 week low
    "k",    // 52 week high
    "o",    // Open price
    "p",    // Previous closing price
    "c1",   // Change
    "p2",   // Change percent
    "d1",   // Last trade date
    "t1",   // Last trade time
    "l1"    // Last trade price
  };

  private String[] __symbols;
  private String[] __columns;

  public static HashMap<String, String>[] fetchStockDetails(String[] symbols, String[] columns) {
    HashMap<String, String>[] response = null;

    try {
      URL url = __buildRequestUrl(symbols, columns);
      HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
      InputStream in = new BufferedInputStream(httpURLConnection.getInputStream());
      String csv = __parseStream(in).trim();
      response = __parseCSV(csv, columns);
    } catch (java.net.MalformedURLException e) {
      return null;
    } catch (java.io.IOException e) {
      return null;
    }

    return response;
  }

  private static URL __buildRequestUrl(String[] symbols, String[] columns) throws java.net.MalformedURLException {
    String cols = StringUtils.join(columns);
    String syms = StringUtils.join(symbols, "+");

    String reqStr = "http://finance.yahoo.com/d/quotes.csv?s=" + syms + "&f=" + cols;
    return new URL(reqStr);
  }

  private static String __parseStream(InputStream in) {
    String s = null;
    try {
      s = IOUtils.toString(in);
    } catch (IOException e) {
      return null;
    }

    return s;
  }

  private static HashMap<String, String>[] __parseCSV(String csv, String[] columns) {
    HashMap<String, String>[] details;

    try {
      CSVReader csvReader = new CSVReader(new StringReader(csv));
      List<String[]> lines = csvReader.readAll();
      details = new HashMap[lines.size()];

      int ndx = 0;
      for (String[] line : lines) {
        HashMap<String, String> info  = new HashMap<>(columns.length);
        for (int i = 0; i < columns.length; i++) {
          info.put(columns[i], line[i]);
        }

        details[ndx] = info;
        ndx++;
      }
    } catch (IOException e) {
      return null;
    }

    return details;
  }
}
