package com.othebe.fantasystock.ui.chart;

import android.content.Context;
import android.graphics.Paint;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v4.view.LayoutInflaterFactory;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.othebe.fantasystock.R;

import rx.functions.Func1;

/**
 * Created by otheb on 4/16/2016.
 */
public class ChartView extends LinearLayout {
  // Time periods for chart.
  private enum Period {
    ONE_DAY,
    ONE_WEEK,
    ONE_MONTH,
    SIX_MONTHS,
    ONE_YEAR
  }

  // Map textView IDs to Periods.
  private static Object[][] __tvToPeriod = {
    { R.id.view_chart_1d, Period.ONE_DAY },
    { R.id.view_chart_1w, Period.ONE_WEEK },
    { R.id.view_chart_1m, Period.ONE_MONTH },
    { R.id.view_chart_6m, Period.SIX_MONTHS },
    { R.id.view_chart_1y, Period.ONE_YEAR }
  };

  private Context __context;
  private Period __period;
  private String __symbol;

  public ChartView(Context context, String symbol) {
    super(context);
    this.__context = context;
    this.__period = Period.ONE_WEEK;
    this.__symbol = symbol;

    this.__initializeLayout();
  }

  /**
   * Initialize layout for view.
   */
  private void __initializeLayout() {
    LayoutInflater layoutInflater = this.__getLayoutInflater();
    layoutInflater.inflate(R.layout.view_chart, this, true);

    this.__updateChart();
    this.__handleTimePeriodClick();
  }

  /**
   * Update period selections.
   */
  private void __updatePeriodSelection() {
    // Set underlines.
    for (Object[] pair: __tvToPeriod) {
      int id = (int) pair[0];
      Period period = (Period) pair[1];
      TextView tv = (TextView) findViewById(id);
      if (period != this.__period) {
        tv.setPaintFlags(tv.getPaintFlags() & (~Paint.UNDERLINE_TEXT_FLAG));
      } else {
        tv.setPaintFlags(tv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
      }
    }
  }

  /**
   * Update chart.
   */
  private void __updateChart() {
    this.__updatePeriodSelection();

    WebView webView = (WebView) findViewById(R.id.view_chart_chart);
    String chartUrl = "http://chart.finance.yahoo.com/z?s=" +
      this.__symbol + "&t=" + periodToString(this.__period) + "&q=l&l=on&z=s&p=m50,m200";
    webView.loadDataWithBaseURL(null, "<style>img{display: inline;height: auto;max-width: 100%;}</style><img src=\"" + chartUrl + "\">", "text/html", "UTF-8", null);
  }

  /**
   * Handle time period clicks.
   */
  private void __handleTimePeriodClick() {
    final ChartView self = this;

    Func1<Period, OnClickListener> getOnClickListener = new Func1<Period, OnClickListener>() {
      @Override
      public OnClickListener call(final Period period) {
        return new OnClickListener() {
          @Override
          public void onClick(View v) {
            if (period == self.__period) {
              return;
            }

            // Update chart.
            self.__period = period;
            self.__updateChart();
          }
        };
      }
    };

    for (Object[] pair: __tvToPeriod) {
      findViewById((int) pair[0]).setOnClickListener(getOnClickListener.call((Period) pair[1]));
    }
  }

  /**
   * Get AppCompat layout inflater.
   * @return
   */
  private LayoutInflater __getLayoutInflater() {
    LayoutInflater result = ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
      .cloneInContext(this.__context);
    LayoutInflaterCompat.setFactory(result, new LayoutInflaterFactory() {
      @Override
      public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        return null;
      }
    });
    return result;
  }

  private static String periodToString(Period period) {
    switch (period) {
      case ONE_DAY:
        return "1d";
      case ONE_WEEK:
        return "1w";
      case ONE_MONTH:
        return "1m";
      case SIX_MONTHS:
        return "6m";
      case ONE_YEAR:
        return "1y";
      default:
        return "1w";
    }
  }
}
