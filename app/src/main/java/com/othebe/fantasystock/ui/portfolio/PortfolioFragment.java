package com.othebe.fantasystock.ui.portfolio;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.othebe.fantasystock.R;
import com.othebe.fantasystock.models.PortfolioItem;
import com.othebe.fantasystock.ui.StockDetailsActivity;

/**
 * Created by otheb on 4/12/2016.
 */
public class PortfolioFragment extends Fragment {
  private PortfolioItem[] __portfolio;

  private double __totalCurrentValue = 0;
  private double __totalValueChange = 0;
  private double __totalPercentChange = 0;
  private double __totalOverallValueChange = 0;
  private double __totalOverallPercentChange = 0;

  private boolean __viewCreated = false;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_portfolio, container, false);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    this.__viewCreated = true;
    this.__drawPortfolio();
  }

  /**
   * Set portfolio to for this fragment.
   * @param portfolio
   */
  public void setPortfolio(PortfolioItem[] portfolio) {
    this.__portfolio = portfolio;
    this.__calculateFooterData();
    this.__drawPortfolio();
  }

  /**
   * Calculate summations to display in the footer.
   */
  private void __calculateFooterData() {
    for (int i = 0; i < this.__portfolio.length; i++) {
      PortfolioItem portfolioItem = this.__portfolio[i];

      this.__totalCurrentValue += portfolioItem.getCurrentValue();
      this.__totalValueChange += portfolioItem.getValueChange();
      this.__totalOverallValueChange += portfolioItem.getOverallChange();
    }

    this.__totalPercentChange = this.__totalValueChange / (this.__totalCurrentValue - this.__totalValueChange) * 100;
    this.__totalOverallPercentChange = this.__totalOverallValueChange / (this.__totalCurrentValue - this.__totalOverallValueChange) * 100;
  }

  /**
   * Draw portfolio table.
   */
  private void __drawPortfolio() {
    if (this.__viewCreated && this.__portfolio != null) {
      this.__drawSymbolTable();
      this.__drawDetailsTable();
    }
  }

  /**
   * Draw symbol table.
   */
  private void __drawSymbolTable() {
    Context context = getContext();
    TableLayout tableLayout = (TableLayout) getView().findViewById(R.id.fragment_portfolio__symbol_table);

    // Add symbol header.
    TableRow headerRow = new TableRow(context);
    __addHeader(context, headerRow, "Symbol");
    tableLayout.addView(headerRow);

    // Create symbols.
    for (int i = 0; i < this.__portfolio.length; i++) {
      PortfolioItem item = this.__portfolio[i];
      String name = item.getName();
      final String symbol = item.getSymbol();

      TableRow row = new TableRow(context);
      row.setBackgroundResource(R.drawable.portfolio_symbol_cell);
      PortfolioSymbolView view = new PortfolioSymbolView(context, symbol, name);
      row.addView(view);
      tableLayout.addView(row);

      final PortfolioFragment self = this;
      view.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          Bundle bundle = new Bundle();
          bundle.putString(StockDetailsActivity.BUNDLE_SYMBOL, symbol);
          Intent i = new Intent(self.getContext(), StockDetailsActivity.class);
          i.putExtras(bundle);
          startActivity(i);
          Log.d("PortfolioFrag", symbol);
        }
      });
    }
  }

  /**
   * Draw details table.
   */
  private void __drawDetailsTable() {
    Context context = getContext();
    TableLayout tableLayout = (TableLayout) getView().findViewById(R.id.fragment_portfolio__details_table);

    __drawHeaders(context, tableLayout);

    for (int i = 0; i < this.__portfolio.length; i++) {
      PortfolioItem item = this.__portfolio[i];
      TableRow row = new TableRow(context);

      // Quantity.
      __addCell(context, row, String.format("%.2f", item.getQuantity()), false);
      // Last price.
      __addCell(context, row, String.format("%.2f", item.getLastPrice()), false);
      // Current value.
      __addCell(context, row, String.format("%.2f", item.getCurrentValue()), false);
      // Value change.
      __addCell(context, row, String.format("%.2f", item.getValueChange()), true);
      // Percent change.
      __addCell(context, row, String.format("%.2f", item.getPercentChange()), true);
      // Overall value change.
      __addCell(context, row, String.format("%.2f", item.getOverallChange()), true);
      // Overall percent change.
      __addCell(context, row, String.format("%.2f", item.getOverallPercentChange()), true);
      // Cost basis.
      __addCell(context, row, String.format("%.2f", item.getCostBasisPerUnit()), false);

      tableLayout.addView(row);
    }

    // Draw footers.
    __drawFooters(context, tableLayout);
  }

  /**
   * Draw headers for portfolio table.
   * @param context
   * @param layout
   */
  private static void __drawHeaders(Context context, TableLayout layout) {
    TableRow row = new TableRow(context);
    __addHeader(context, row, context.getString(R.string.portfolio_label_quantity));
    __addHeader(context, row, context.getString(R.string.portfolio_label_last_price));
    __addHeader(context, row, context.getString(R.string.portfolio_label_current_value));
    __addHeader(context, row, context.getString(R.string.portfolio_label_value_change));
    __addHeader(context, row, context.getString(R.string.portfolio_label_percent_change));
    __addHeader(context, row, context.getString(R.string.portfolio_label_overall_value_change));
    __addHeader(context, row, context.getString(R.string.portfolio_label_overall_percent_change));
    __addHeader(context, row, context.getString(R.string.portfolio_label_cost_basis_per_unit));

    layout.addView(row);
  }

  /**
   * Draw footers for portfolio table.
   * @param context
   * @param layout
   */
  private void __drawFooters(Context context, TableLayout layout) {
    TableRow row = new TableRow(context);
    __addFooter(context, row, null, false);
    __addFooter(context, row, null, false);
    __addFooter(context, row, String.format("%.2f", this.__totalCurrentValue), true);
    __addFooter(context, row, String.format("%.2f", this.__totalValueChange), true);
    __addFooter(context, row, String.format("%.2f", this.__totalPercentChange), true);
    __addFooter(context, row, String.format("%.2f", this.__totalOverallValueChange), true);
    __addFooter(context, row, String.format("%.2f", this.__totalOverallPercentChange), true);
    __addFooter(context, row, null, false);

    layout.addView(row);
  }

  /**
   * Add a header cell to a TableRow.
   * @param context
   * @param row
   * @param text
   */
  private static void __addHeader(Context context, TableRow row, String text) {
    row.addView(new PortfolioItemView(context, text, PortfolioItemView.CellType.HEADER, false));
  }

  /**
   * Add a footer cell to a TableRow.
   * @param context
   * @param row
   * @param text
   */
  private static void __addFooter(Context context, TableRow row, String text, boolean showIndicator) {
    row.addView(new PortfolioItemView(context, text, PortfolioItemView.CellType.FOOTER, showIndicator));
  }

  /**
   * Add a regular cell to a TableRow.
   * @param context
   * @param row
   * @param text
   */
  private static void __addCell(Context context, TableRow row, String text, boolean showIndicator) {
    row.addView(new PortfolioItemView(context, text, PortfolioItemView.CellType.DATA, showIndicator));
  }


}
