package com.othebe.fantasystock.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.othebe.fantasystock.R;
import com.othebe.fantasystock.models.PortfolioItem;
import com.othebe.fantasystock.ui.portfolio.IPortfolioView;
import com.othebe.fantasystock.ui.portfolio.PortfolioFragment;
import com.othebe.fantasystock.ui.portfolio.PortfolioPresenter;

/**
 * Created by otheb on 4/17/2016.
 */
public class PortfolioActivity extends BaseDrawerActivity implements IPortfolioView {
  private PortfolioPresenter __portfolioPresenter;

  @Override
  int getLayoutId() {
    return R.layout.content_portfolio;
  }

  @Override
  void onAfterCreate(Bundle savedInstanceState) {
    this.__portfolioPresenter = new PortfolioPresenter(this);
  }

  @Override
  public void attachPortfolio(PortfolioItem[] portfolio) {
    PortfolioFragment portfolioFragment = new PortfolioFragment();
    portfolioFragment.setPortfolio(portfolio);

    if (portfolio.length == 0) {
      this.showEmptyUi();
    } else {
      this.hideEmptyUi();
      FragmentManager fragmentManager = getSupportFragmentManager();
      FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
      fragmentTransaction.add(R.id.portfolio__fragment_container, portfolioFragment);
      fragmentTransaction.commit();
    }
  }

  @Override
  public void showEmptyUi() {
    findViewById(R.id.portfolio__list).setVisibility(View.GONE);
    findViewById(R.id.portfolio__empty).setVisibility(View.VISIBLE);
  }

  @Override
  public void hideEmptyUi() {
    findViewById(R.id.portfolio__list).setVisibility(View.VISIBLE);
    findViewById(R.id.portfolio__empty).setVisibility(View.GONE);
  }
}
