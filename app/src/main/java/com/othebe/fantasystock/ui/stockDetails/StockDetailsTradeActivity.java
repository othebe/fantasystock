package com.othebe.fantasystock.ui.stockDetails;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import com.othebe.fantasystock.R;
import com.othebe.fantasystock.common.ValidationResult;
import com.othebe.fantasystock.constants.TransactionType;
import com.othebe.fantasystock.models.Stock;
import com.othebe.fantasystock.ui.StockDetailsActivity;
import com.othebe.fantasystock.ui.SummaryActivity;

import java.util.ArrayList;
import java.util.HashMap;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by otheb on 4/17/2016.
 */
public class StockDetailsTradeActivity extends Activity implements IStockDetailsTradeView {
  private Stock __stock;
  private StockDetailsTradePresenter __presenter;
  private Subscription __tickerSubscription;

  private SimpleAdapter __adapter;
  private ArrayList<HashMap<String, String>> __adapterList;
  private static final String ID_DATA_KEY = "id-data-key";
  private static final String ID_DATA_VAL = "id-data-val";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    this.__stock = getIntent().getExtras().getParcelable(StockDetailsActivity.BUNDLE_STOCK);

    setContentView(R.layout.tab_stock_details_trade);

    this.__initializeOrderTypeSpinners();
    this.__initializeAdapter();
    this.__presenter = new StockDetailsTradePresenter(this, this.__stock);
    this.__handleOrderQtyChange();
    this.__handleOrderBtn();

    this.__tickerSubscription = this.__stock.getTicker()
      .getObservable()
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this.__getTickerUpdateHandler());
  }

  @Override
  protected void onPause() {
    super.onPause();
    this.__tickerSubscription.unsubscribe();
  }

  private void __initializeOrderTypeSpinners() {
    String[] orderTypes = { getString(R.string.trade_order_type_buy), getString(R.string.trade_order_type_sell) };
    Spinner spinner = (Spinner) findViewById(R.id.tab_stock_details__trade_order_type);
    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, orderTypes);
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    spinner.setAdapter(adapter);

    final StockDetailsTradeActivity self = this;
    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0) {
          self.__presenter.setOrderType(TransactionType.BUY);
        } else {
          self.__presenter.setOrderType(TransactionType.SELL);
        }
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) { }
    });
  }

  private void __initializeAdapter() {
    this.__adapterList = new ArrayList<>();
    int resId = R.layout.item_simple_list;
    String[] from = { ID_DATA_KEY, ID_DATA_VAL };
    int[] to = { R.id.simple_list_item__key, R.id.simple_list_item__value };
    this.__adapter = new SimpleAdapter(this, this.__adapterList, resId, from, to);

    ListView listview = (ListView) findViewById(R.id.tab_stock_details__trade_preview);
    listview.setAdapter(this.__adapter);
  }

  // Refresh stock and update view.
  private Action1 __getTickerUpdateHandler() {
    final StockDetailsTradeActivity self = this;
    return new Action1<Object>() {
      @Override
      public void call(Object ignore) {
        self.updateView();
      }
    };
  }

  private void __handleOrderQtyChange() {
    final StockDetailsTradeActivity self = this;
    EditText qty = (EditText) findViewById(R.id.tab_stock_details__trade_order_qty);
    qty.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) { }

      @Override
      public void afterTextChanged(Editable s) {
        int units = 0;
        String str = s.toString();
        if (str.length() > 0) {
          try {
            units = Integer.valueOf(str);
          } catch (Exception e) { }
        }

        self.__presenter.setNumUnits(units);
      }
    });
  }

  private void __handleOrderBtn() {
    final StockDetailsTradeActivity self = this;
    Button btn = (Button) findViewById(R.id.tab_stock_details__trade_place_order_btn);
    btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        ValidationResult validationResult = self.__presenter.validate();
        // Show error.
        if (!validationResult.isSuccess()) {
          AlertDialog.Builder builder = new AlertDialog.Builder(self);
          builder
            .setTitle(R.string.error_transaction_error)
            .setMessage(validationResult.getDetails())
            .setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                return;
              }
            }).show();
        }
        // Success.
        else {
          self.__presenter.placeOrder();
          Intent i = new Intent(self, SummaryActivity.class);
          startActivity(i);
        }
      }
    });
  }

  @Override
  public void updateView() {
    Object[][] src = {
      { R.string.trade_preview_stock_price, this.__presenter.getStockPrice() },
      { R.string.trade_preview_available_cash, this.__presenter.getAvailableCash() },
      { R.string.trade_preview_units_owned, this.__presenter.getNumUnitsOwned() },
      { R.string.trade_preview_total_stock_price, this.__presenter.getTotalStockPrice() },
      { R.string.trade_preview_transaction_cost, this.__presenter.getTransactionCost() },
      { R.string.trade_preview_total_cost, this.__presenter.getTotalPrice() },
      { R.string.trade_preview_available_cash_after, this.__presenter.getAvailableCashAfterTransaction() },
      { R.string.trade_preview_available_units_after, this.__presenter.getUnitsAfterTransaction() }
    };
    this.__adapterList.clear();
    for (Object[] o : src) {
      this.__adapterList.add(HashMap(getString((int) o[0]), safeParse((double) o[1])));
    }
    this.__adapter.notifyDataSetChanged();
  }

  private static String safeParse(double val) {
    try {
      return String.format("%.2f", val);
    } catch(Exception e) { }
    return "";
  }

  /**
   * Create a hashmap with key/values.
   * @param key
   * @param value
   * @return
   */
  private static HashMap<String, String> HashMap(String key, String value) {
    HashMap<String, String> newMap = new HashMap<>();
    newMap.put(ID_DATA_KEY, key);
    newMap.put(ID_DATA_VAL, value);
    return newMap;
  }
}
