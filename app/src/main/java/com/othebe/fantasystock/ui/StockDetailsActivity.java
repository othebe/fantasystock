package com.othebe.fantasystock.ui;

import android.app.LocalActivityManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TabHost;

import com.othebe.fantasystock.R;
import com.othebe.fantasystock.models.Stock;
import com.othebe.fantasystock.ui.stockDetails.StockDetailsInfoActivity;
import com.othebe.fantasystock.ui.stockDetails.StockDetailsTradeActivity;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class StockDetailsActivity extends BaseDrawerActivity {
  public static final String BUNDLE_SYMBOL = "bundle-symbol";
  public static final String BUNDLE_STOCK = "bundle-stock";

  private String __symbol;
  private Stock __stock;
  private Subscription __tickerSubscription;

  private LocalActivityManager __localActivityManager;

    @Override
  int getLayoutId() {
    return R.layout.content_stock_details;
  }

  @Override
  void onAfterCreate(Bundle savedInstanceState) {
    this.__symbol = getIntent().getExtras().getString(BUNDLE_SYMBOL);

    this.setTitle(this.__symbol);

    this.__localActivityManager = new LocalActivityManager(this, true);
    this.__localActivityManager.dispatchCreate(savedInstanceState);

    // TODO: Can we pass stock to tabbed activities?
    final StockDetailsActivity self = this;
    Stock stock = new Stock(this.__symbol);
    stock.refresh()
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(new Action1<Stock>() {
        @Override
        public void call(Stock stock) {
          self.__stock = stock;
          self.setTitle(stock.getSymbol());
          self.setSubtitle(stock.getName());
          self.__initializeTicker();
          self.__createTabs();
        }
      });
  }

  @Override
  protected void onPause() {
    super.onPause();
    this.__tickerSubscription.unsubscribe();
    this.__localActivityManager.dispatchPause(isFinishing());
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    this.__localActivityManager.dispatchDestroy(isFinishing());
  }

  @Override
  protected void onStop() {
    super.onStop();
    this.__localActivityManager.dispatchStop();
  }

  @Override
  protected void onResume() {
    super.onResume();
    this.__localActivityManager.dispatchResume();
  }

  // Keep a subscription open so the ticker is a hot observable.
  // TODO: Update the tabbed activities when ticker updates.
  private void __initializeTicker() {
    this.__tickerSubscription = this.__stock.getTicker()
      .getObservable()
      .subscribeOn(Schedulers.io())
      .subscribe();
  }

  private void __createTabs() {
    Object[][] tabData = {
      { getString(R.string.stock_details_tab_info), StockDetailsInfoActivity.class },
      { getString(R.string.stock_details_tab_trade), StockDetailsTradeActivity.class }
    };

    TabHost tabHost = (TabHost) findViewById(R.id.stock_details__tab_host);
    tabHost.setup(this.__localActivityManager);

    for (Object[] o : tabData) {
      String tabName = (String) o[0];
      Class klass = (Class) o[1];

      TabHost.TabSpec tabSpec = tabHost.newTabSpec(tabName);
      tabSpec.setIndicator(tabName);

      Intent i = new Intent(this, klass);
      Bundle bundle = new Bundle();
      bundle.putParcelable(BUNDLE_STOCK, this.__stock);
      i.putExtras(bundle);
      tabSpec.setContent(i);

      tabHost.addTab(tabSpec);
    }
  }
}

