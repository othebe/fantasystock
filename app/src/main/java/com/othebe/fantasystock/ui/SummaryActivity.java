package com.othebe.fantasystock.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.othebe.fantasystock.R;
import com.othebe.fantasystock.models.Portfolio;
import com.othebe.fantasystock.models.PortfolioItem;
import com.othebe.fantasystock.models.Position;
import com.othebe.fantasystock.models.Stock;
import com.othebe.fantasystock.models.User;
import com.othebe.fantasystock.ui.portfolio.PortfolioFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Callable;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class SummaryActivity extends BaseDrawerActivity {
  private double __cashAvailable;
  private Portfolio __portfolio;

  @Override
  int getLayoutId() {
    return R.layout.content_summary;
  }

  @Override
  void onAfterCreate(Bundle savedInstanceState) {
    final SummaryActivity self = this;
    Observable
      .fromCallable(this.__initializeCallable())
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(new Action1() {
        @Override
        public void call(Object o) {
          self.__onInitialized();
        }
      });
  }

  private Callable __initializeCallable() {
    final SummaryActivity self = this;
    return new Callable() {
      @Override
      public Object call() throws Exception {
        self.__cashAvailable = User.getCurrent().getAvailableCash(self);
        self.__portfolio = Portfolio.buildPortfolio(self);

        return null;
      }
    };
  }

  private void __onInitialized() {
    this.__populateBalanceSummary();
    this.__initializeDetails();
  }

  private void __populateBalanceSummary() {
    TextView balanceTv = (TextView) findViewById(R.id.summary__balance);
    double valuation = this.__cashAvailable + this.__portfolio.getCurrentValuation();
    balanceTv.setText(String.format("%.2f", valuation));

    TextView changeValueTv = (TextView) findViewById(R.id.summary__change_value);
    double changeValue = this.__portfolio.getTodaysValueChange();
    changeValueTv.setText(String.format("%.2f", changeValue));

    TextView changePercentTv = (TextView) findViewById(R.id.summary__change_percent);
    double changePercent = this.__portfolio.getTodaysPercentChange();
    changePercentTv.setText("(" + String.format("%.2f", changePercent) + "%)");

    if (changeValue > 0) {
      changeValueTv.setTextColor(Color.GREEN);
      changePercentTv.setTextColor(Color.GREEN);
    } else if (changeValue < 0) {
      changeValueTv.setTextColor(Color.RED);
      changePercentTv.setTextColor(Color.RED);
    }
  }

  private void __initializeDetails() {
    Object[][] src = {
      { R.string.summary_todays_change_value, this.__portfolio.getTodaysValueChange() },
      { R.string.summary_todays_change_percent, this.__portfolio.getTodaysPercentChange() },
      { R.string.summary_overall_change_value, this.__portfolio.getOverallValueChange() },
      { R.string.summary_overall_change_percent, this.__portfolio.getOverallPercentChange() },
      { R.string.summary_cash_to_trade, this.__cashAvailable },
      { R.string.summary_investment_costs, this.__portfolio.getInvestmentPurchaseAmount() },
      { R.string.summary_current_valuation, this.__portfolio.getCurrentValuation() }
    };
    String[] from = { "key", "value" };
    ArrayList<HashMap<String, String>> data = new ArrayList<>();
    for (Object[] o : src) {
      String key = getString((int) o[0]);
      String value = String.format("%.2f", (double) o[1]);
      data.add(HashMap(key, value, from));
    }

    int resId = R.layout.item_simple_list;
    int[] to = { R.id.simple_list_item__key, R.id.simple_list_item__value };

    SimpleAdapter adapter = new SimpleAdapter(this, data, resId, from, to);

    ListView list = (ListView) findViewById(R.id.summary__list);
    list.setAdapter(adapter);
  }

  private void __initializeListAdaper() {

  }

  private static HashMap<String, String> HashMap(String key, String value, String[] from) {
    HashMap<String, String> result = new HashMap<>();
    result.put(from[0], key);
    result.put(from[1], value);
    return result;
  }



  /**
   * Initialize portfolio fragment.
   */
//  private void __initPortfolio() {
//    this.__portfolio = new ArrayList<>();
//    final SummaryActivity self = this;
//
//    Position[] positions = Position.getAllPositions(this);
//
//    // Get positions.
//    Callable<Position[]> getPositions = new Callable<Position[]>() {
//      @Override
//      public Position[] call() throws Exception {
//        return Position.getAllPositions(self);
//      }
//    };
//
//    Action1<Position[]> getStocksForPositions = new Action1<Position[]>() {
//      @Override
//      public void call(Position[] positions) {
//
//      }
//    };
//
//    Observable
//      .fromCallable(getPositions)
//      .subscribeOn(Schedulers.io())
//      .subscibe()
//
//    // Create stock from symbol array.
//
//
//    // Get stock information from positions.
//    Callable<PortfolioItem[]> getStockDetailsForPositions = new Callable<PortfolioItem[]>() {
//      @Override
//      public PortfolioItem[] call() throws Exception {
//        Position[] positions = Position.getAllPositions(self);
//        PortfolioItem[] portfolio = new PortfolioItem[positions.length];
//        for (int i = 0; i < positions.length; i++) {
//          Position position = positions[i];
//          Stock stock = new Stock(position.getSymbol(), false);
//          portfolio[i] = new PortfolioItem(stock, position);
//        }
//        return portfolio;
//      }
//    };
//
//    // Initialize portfolio fragment.
//    Action1<PortfolioItem[]> handlePortfolioLoaded = new Action1<PortfolioItem[]>() {
//      @Override
//      public void call(PortfolioItem[] portfolio) {
//        self.__createPortfolioFragment(portfolio);
//      }
//    };
//
//    Observable
//      .fromCallable(getStocksForPositions)
//      .subscribeOn(Schedulers.io())
//      .observeOn(AndroidSchedulers.mainThread())
//      .subscribe(handlePortfolioLoaded);
//  }

//  private void __createPortfolioFragment(PortfolioItem[] portfolio) {
//    PortfolioFragment portfolioFragment = new PortfolioFragment();
//    portfolioFragment.setPortfolio(portfolio);
//
//    FragmentManager fragmentManager = getSupportFragmentManager();
//    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//    fragmentTransaction.add(R.id.summary__portfolio_container, portfolioFragment);
//    fragmentTransaction.commit();
//  }
}
