package com.othebe.fantasystock.ui.stockDetails;

import android.content.Context;

import com.othebe.fantasystock.R;
import com.othebe.fantasystock.common.ValidationResult;
import com.othebe.fantasystock.constants.Constants;
import com.othebe.fantasystock.constants.TransactionType;
import com.othebe.fantasystock.models.Position;
import com.othebe.fantasystock.models.Stock;
import com.othebe.fantasystock.models.Transaction;
import com.othebe.fantasystock.models.User;
/**
 * Created by otheb on 4/18/2016.
 */
public class StockDetailsTradePresenter {
  private IStockDetailsTradeView __view;

  private Stock __stock;
  private TransactionType __transactionType;
  private double __numUnits = 0.0;
  private Position __position;
  private double __availableCash = -1.0;

  public StockDetailsTradePresenter(IStockDetailsTradeView view, Stock stock) {
    this.__view = view;
    this.__stock = stock;
  }

  public void setOrderType(TransactionType transactionType) {
    this.__transactionType = transactionType;
    this.__updateView();
  }

  public void setNumUnits(int numUnits) {
    this.__numUnits = numUnits;
    this.__updateView();
  }

  public double getStockPrice() {
    return this.__stock.getLastTradePrice();
  }

  public double getAvailableCash() {
    if (this.__availableCash >= 0) {
      return this.__availableCash;
    } else {
      this.__availableCash = User.getCurrent().getAvailableCash(((Context) this.__view));
      return this.__availableCash;
    }
  }

  public double getNumUnitsOwned() {
    if (this.__position == null) {
      this.__position = Position.findBySymbol((Context) this.__view, this.__stock.getSymbol());
    }
    if (this.__position == null) {
      return 0;
    } else {
      return this.__position.getUnits();
    }
  }

  public double getTotalStockPrice() {
    return this.__numUnits * this.getStockPrice();
  }

  public double getTransactionCost() {
    return Constants.TRANSACTION_COST;
  }

  public double getTotalPrice() {
    if (this.__transactionType == TransactionType.BUY) {
      return this.getTotalStockPrice() + this.getTransactionCost();
    } else {
      return this.getTotalStockPrice() - this.getTransactionCost();
    }
  }

  public double getUnitsAfterTransaction() {
    if (this.__transactionType == TransactionType.BUY) {
      return this.getNumUnitsOwned() + this.__numUnits;
    } else {
      return this.getNumUnitsOwned() - this.__numUnits;
    }
  }

  public double getAvailableCashAfterTransaction() {
    if (this.__transactionType == TransactionType.BUY) {
      return this.getAvailableCash() - this.getTotalPrice();
    } else {
      return this.getAvailableCash() + this.getTotalPrice();
    }
  }

  public ValidationResult validate() {
    boolean isSuccess = true;
    String details = "";

    // Invalid units.
    if (this.__numUnits <= 0) {
      isSuccess = false;
      details = ((Context) this.__view).getString(R.string.error_invalid_units);
    }
    // Insufficient balance.
    else if (this.getAvailableCashAfterTransaction() < 0) {
      isSuccess = false;
      details = ((Context) this.__view).getString(R.string.error_insufficient_balance);
    }
    // Insufficient units.
    else if (this.getUnitsAfterTransaction() < 0) {
      isSuccess = false;
      details = ((Context) this.__view).getString(R.string.error_insufficient_units);
    }

    return new ValidationResult(isSuccess, details);
  }

  public void placeOrder() {
    // TODO: Can this be placed in an SQL Transaction?
    Context context = (Context) this.__view;
    // Create a transaction.
    Transaction.createTransaction(context, this.__stock, this.__transactionType, this.__numUnits);
    // Update positions.
    Position position = Position.findBySymbol(context, this.__stock.getSymbol());
    if (position == null) {
      position = new Position(this.__stock.getSymbol(), 0, 0);
    }
    position.updateByTransactionType(context, this.__stock, this.__numUnits, this.__transactionType);
    // Update user cash.
    int op = (this.__transactionType == TransactionType.BUY) ? -1 : 1;
    User.getCurrent().addRemoveCash(context, this.getTotalPrice() * op);
  }

  private void __updateView() {
    this.__view.updateView();
  }
}
