package com.othebe.fantasystock.ui.stockDetails;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.othebe.fantasystock.R;
import com.othebe.fantasystock.models.Stock;
import com.othebe.fantasystock.ui.StockDetailsActivity;
import com.othebe.fantasystock.ui.chart.ChartView;

/**
 * Created by otheb on 4/17/2016.
 */
public class StockDetailsInfoActivity extends AppCompatActivity {
  private Stock __stock;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    final StockDetailsInfoActivity self = this;

    setContentView(R.layout.tab_stock_details_info);

    Bundle bundle = getIntent().getExtras();
    this.__stock = bundle.getParcelable(StockDetailsActivity.BUNDLE_STOCK);

    self.__attachTickerFragment();
    self.__attachChartView();
    self.__populateDetails();
  }

  private void __attachTickerFragment() {
    TickerFragment tickerFragment = new TickerFragment();
    tickerFragment.setStock(this.__stock);

    FragmentManager fragmentManager = getSupportFragmentManager();
    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
    fragmentTransaction.add(R.id.tab_stock_details__ticker_container, tickerFragment);
    fragmentTransaction.commit();
  }

  private void __attachChartView() {
    ChartView chartView = new ChartView(this, this.__stock.getSymbol());
    LinearLayout container = (LinearLayout) findViewById(R.id.tab_stock_details__chart_container);
    container.addView(chartView);
  }

  private void __populateDetails() {
    Object[][] sources = {
      { R.string.stock_details_day_low_label, this.__stock.getDayLow() },
      { R.string.stock_details_day_high_label, this.__stock.getDayHigh() },
      { R.string.stock_details_52_low_label, this.__stock.get52WeekLow() },
      { R.string.stock_details_52_high_label, this.__stock.get52WeekHigh() },
      { R.string.stock_details_opening_label, this.__stock.getOpeningPrice() },
      { R.string.stock_details_day_close_label, this.__stock.getClosingPrice() }
    };

    TableRow.LayoutParams layoutParams = new TableRow.LayoutParams();
    layoutParams.weight = 1;

    TableLayout tableLayout = (TableLayout) findViewById(R.id.tab_stock_details__info_table);
    for (Object o[] : sources) {
      String label = getString((int) o[0]);
      String value = safeParse(o[1]);

      TableRow tableRow = new TableRow(this);

      TextView labelTv = new TextView(this);
      labelTv.setText(label);
      labelTv.setLayoutParams(layoutParams);
      tableRow.addView(labelTv);

      TextView valueTv = new TextView(this);
      valueTv.setText(value);
      tableRow.addView(valueTv);

      tableLayout.addView(tableRow);
    }
  }

  private static String safeParse(Object o) {
    try {
      return String.format("%.2f", o);
    } catch (Exception e) {
      return o.toString();
    }
  }
}
