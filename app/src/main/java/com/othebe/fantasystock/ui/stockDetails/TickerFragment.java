package com.othebe.fantasystock.ui.stockDetails;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.othebe.fantasystock.R;
import com.othebe.fantasystock.models.Stock;
import com.othebe.fantasystock.ticker.Ticker;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;


/**
 * Created by otheb on 4/5/2016.
 */
public class TickerFragment extends Fragment {
  public static final String ID_BUNDLE_SYMBOL = "id-bundle-symbol";
  public static final String ID_BUNDLE_ASK_PRICE = "id-ask-price";
  public static final String ID_BUNDLE_CHANGE = "id-change";
  public static final String ID_BUNDLE_CHANGE_PERCENT = "id-change-percent";
  public static final String ID_BUNDLE_LAST_DATE = "id-last-date";
  public static final String ID_BUNDLE_LAST_TIME = "id-last-time";
  public static final String ID_BUNDLE_TIMESTAMP = "id-timestamp";

  private Stock __stock;
  private Ticker __ticker;
  private Subscription __tickerSubscription;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_ticker, container, false);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (this.__stock != null) {
      this.__bindStockToView();
    }
  }

  @Override
  public void onPause() {
    super.onPause();
    this.__tickerSubscription.unsubscribe();
  }

  public void setStock(Stock stock) {
    this.__stock = stock;
    this.__ticker = stock.getTicker();
  }

  private void __bindStockToView() {
    final TickerFragment self = this;
    this.__tickerSubscription = this.__ticker.getObservable()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(new Action1() {
        @Override
        public void call(Object o) {
          self.__updateView();
        }
      });
  }

  private void __updateView() {
    TextView symbolTv = (TextView) getView().findViewById(R.id.fragment_ticker__symbol);
    symbolTv.setText(this.__stock.getSymbol());

    TextView askPriceTv = (TextView) getView().findViewById(R.id.fragment_ticker__ask_price);
    askPriceTv.setText(String.format("%.2f", this.__stock.getLastTradePrice()));

    TextView changeTv = (TextView) getView().findViewById(R.id.fragment_ticker__change);
    changeTv.setText(String.format("%.2f", this.__stock.getChange()));

    TextView changePercentTv = (TextView) getView().findViewById(R.id
      .fragment_ticker__change_percent);
    changePercentTv.setText("(" + this.__stock.getChangePercent() + ")");

    String datetimeText = getContext().getString(R.string.stock_last_updated)
      + " " + this.__stock.getLastTradeDate() + " " + this.__stock.getLastTradeTime();
    TextView datetimeTv = (TextView) getView().findViewById(R.id.fragment_ticker__datetime);
    datetimeTv.setText(datetimeText);
  }
}
