package com.othebe.fantasystock.ui.portfolio;

import android.content.Context;

import com.othebe.fantasystock.models.PortfolioItem;
import com.othebe.fantasystock.models.Position;
import com.othebe.fantasystock.models.Stock;

import java.util.concurrent.Callable;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by otheb on 4/17/2016.
 */
public class PortfolioPresenter {
  private IPortfolioView __view;
  private PortfolioItem[] __portfolio;

  public PortfolioPresenter(IPortfolioView view) {
    this.__view = view;

    // Build portfolio.
    final PortfolioPresenter self = this;
    __buildPortfolioObservable((Context) view)
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(new Action1<PortfolioItem[]>() {
        @Override
        public void call(PortfolioItem[] portfolio) {
          self.__portfolio = portfolio;
          self.__view.attachPortfolio(portfolio);
        }
      });
  }

  private static Observable<PortfolioItem[]> __buildPortfolioObservable(final Context context) {
    // Fetch positions from repository.
    Callable<Position[]> getPositions = new Callable<Position[]>() {
      @Override
      public Position[] call() throws Exception {
        return Position.getAllPositions(context);
      }
    };

    Func1<Position[], PortfolioItem[]> positionToPortfolioItem = new Func1<Position[], PortfolioItem[]>() {
      @Override
      public PortfolioItem[] call(Position[] positions) {
        String[] symbols = new String[positions.length];
        for (int i = 0; i < symbols.length; i++) {
          symbols[i] = positions[i].getSymbol();
        }
        Stock[] stocks = Stock.createStocksFromSymbols(symbols);

        PortfolioItem[] portfolio = new PortfolioItem[positions.length];
        for (int i = 0; i < portfolio.length; i++) {
          portfolio[i] = new PortfolioItem(stocks[i], positions[i]);
        }

        return portfolio;
      }
    };

    return Observable
      .fromCallable(getPositions)
      .map(positionToPortfolioItem);
  };
}
