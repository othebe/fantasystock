package com.othebe.fantasystock.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.othebe.fantasystock.R;
import com.othebe.fantasystock.constants.TransactionType;
import com.othebe.fantasystock.models.Position;
import com.othebe.fantasystock.models.Stock;
import com.othebe.fantasystock.models.Transaction;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by otheb on 4/4/2016.
 */
public class TransactionPreviewActivity extends BaseDrawerActivity {
  private static final String LOG_TAG = "TransactionPreview";
  private static final String ID_DATA_KEY = "id-data-key";
  private static final String ID_DATA_VAL = "id-data-val";

  public static final String PARCELABLE_TRANSACTION_TYPE_KEY = "transaction-type-parcelable-key";
  public static final String PARCELABLE_UNITS_KEY = "units-key";

  private Stock __stock;
  private TransactionType __transactionType;
  private float __units;

  @Override
  int getLayoutId() {
    return R.layout.content_transaction_preview;
  }

  @Override
  void onAfterCreate(Bundle savedInstanceState) {
    Bundle bundle = getIntent().getExtras();
    this.__stock = bundle.getParcelable(Stock.PARCELABLE_KEY);
    this.__units = bundle.getFloat(PARCELABLE_UNITS_KEY);
    this.__transactionType = TransactionType.valueOf(bundle.getString(PARCELABLE_TRANSACTION_TYPE_KEY));

    this.__setupAdapter();
    this.__handleActionButton();
  }

  private void __handleActionButton() {
//    final TransactionPreviewActivity self = this;
//
//    Button actionBtn = (Button) findViewById(R.id.transaction_preview__action_btn);
//    actionBtn.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View v) {
//        // TODO: Can we add this to a SQL Transaction?
//
//        // Create a transaction.
//        Transaction.createTransaction(self, self.__stock, self.__transactionType, self.__units);
//        // Update position.
//        Position existing = Position.findBySymbol(self, self.__stock.getSymbol());
//        self.__updatePosition(existing);
//
//        self.__goHome();
//      }
//    });
  }

  /**
   * Provides a callable that updates the position repository.
   * @return
   */
  private void __updatePosition(Position position) {
    if (position == null) {
      position = new Position(this.__stock.getSymbol(), 0, 0);
    }
    position.updateByTransactionType(this, this.__stock, this.__units, this.__transactionType);
  }

  /**
   * Setup adapter.
   */
  private void __setupAdapter() {
    ArrayList<HashMap<String, String>> list = new ArrayList<>();
    Object[][] lines = {
      { R.string.available_cash, "ABC" },
      { R.string.units_owned, "123" },
      { R.string.transaction_units_ordered, "123" },
      { R.string.transaction_price_per_unit, "123" },
      { R.string.transaction_total_unit_price, "123" },
      { R.string.transaction_transaction_cost, "123" },
      { R.string.transaction_total_cost, 234 },
      { R.string.transaction_units_after_transaction, 345 },
      { R.string.transaction_cash_after_transaction, 4566 }
    };
    for (Object[] line : lines) {
      String key = this.getString((int) line[0]);
      String val = String.valueOf(line[1]);
      list.add(HashMap(key, val));
    }

    String[] from = { ID_DATA_KEY, ID_DATA_VAL };
    int[] to = { R.id.simple_list_item__key, R.id.simple_list_item__value };
    int resId = R.layout.item_simple_list;

    SimpleAdapter simpleAdapter = new SimpleAdapter(this, list, resId, from, to);
    ListView listView = (ListView) findViewById(R.id.transaction_preview__list_container);
    listView.setAdapter(simpleAdapter);
  }

  /**
   * Return to summary activity.
   */
  private void __goHome() {
    Intent i = new Intent(this, SummaryActivity.class);
    startActivity(i);
  }

  /**
   * Create a hashmap with key/values.
   * @param key
   * @param value
   * @return
   */
  private static HashMap<String, String> HashMap(String key, String value) {
    HashMap<String, String> newMap = new HashMap<>();
    newMap.put(ID_DATA_KEY, key);
    newMap.put(ID_DATA_VAL, value);
    return newMap;
  }
}
