package com.othebe.fantasystock.ui;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import com.othebe.fantasystock.R;

import rx.functions.Action1;
import rx.functions.Func0;

public abstract class BaseDrawerActivity extends AppCompatActivity
  implements NavigationView.OnNavigationItemSelectedListener {

  /** Gets the layout ID for the implementing activity. */
  abstract int getLayoutId();

  /** Called after the onCreate method. */
  abstract void onAfterCreate(Bundle savedInstanceState);

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.base_layout);
    this.__initializeLayout();

    onAfterCreate(savedInstanceState);
  }

  @Override
  public void onBackPressed() {
    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.app__drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else {
      super.onBackPressed();
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.toolbar, menu);

    // Associate searchable configuration with the SearchView
    SearchManager searchManager =
      (SearchManager) getSystemService(Context.SEARCH_SERVICE);
    SearchView searchView =
      (SearchView) menu.findItem(R.id.toolbar__action_search).getActionView();
    ComponentName componentName = getComponentName();
    searchView.setSearchableInfo(
      searchManager.getSearchableInfo(componentName));

    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    return super.onOptionsItemSelected(item);
  }

  @SuppressWarnings("StatementWithEmptyBody")
  @Override
  public boolean onNavigationItemSelected(MenuItem item) {
    // Handle navigation view item clicks here.
    int id = item.getItemId();

    Intent intent = null;
    switch(id) {
      case R.id.action_summary:
        intent = new Intent(this, SummaryActivity.class);
        break;
      case R.id.action_portfolio:
        intent = new Intent(this, PortfolioActivity.class);
        break;
      case R.id.action_settings:
        intent = new Intent(this, OptionsActivity.class);
        break;
    }

    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.app__drawer_layout);
    drawer.closeDrawer(GravityCompat.START);

    if (intent != null) {
      startActivity(intent);
    }

    return true;
  }

  private void __initializeLayout() {
    // Initialize content.
    this.__initContent();

    // Initialize app bar.
    this.__initAppBar();

    // Initialize drawer.
    this.__initDrawer();
  }

  final protected void setTitle(String title) {
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    toolbar.setTitle(title);
  }

  final protected void setSubtitle(String subtitle) {
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    toolbar.setSubtitle(subtitle);
  }

  protected void displayAlert(String text) {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setMessage(text);
    builder.show();
  }

  /**
   * Initialize content for implementing class.
   */
  private void __initContent() {
    LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(this
      .LAYOUT_INFLATER_SERVICE);
    RelativeLayout contentLayout = (RelativeLayout) findViewById(R.id.base_layout__content_layout);
    layoutInflater.inflate(getLayoutId(), contentLayout);
  }

  /**
   * Sets the appbar.
   */
  private void __initAppBar() {
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
  }

  /**
   * Initialize the drawer.
   */
  private void __initDrawer() {
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.app__drawer_layout);
    ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
      this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    drawer.setDrawerListener(toggle);
    toggle.syncState();

    // Setup drawer navigation listeners.
    NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
    navigationView.setNavigationItemSelectedListener(this);
  }
}
