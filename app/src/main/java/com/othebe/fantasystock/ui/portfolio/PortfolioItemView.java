package com.othebe.fantasystock.ui.portfolio;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v4.view.LayoutInflaterFactory;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.othebe.fantasystock.R;

/**
 * Created by otheb on 4/13/2016.
 */
public class PortfolioItemView extends RelativeLayout {
  public static enum CellType {
    DATA,
    FOOTER,
    HEADER,
    SYMBOL
  }

  private Context __context;
  private CellType __cellType;
  private boolean __showIndicator;

  private TextView __textTv;

  public PortfolioItemView(Context context, String text, CellType celltype, boolean showIndicator) {
    super(context);

    this.__context = context;
    this.__cellType = celltype;
    this.__showIndicator = showIndicator;

    this.__initializeView(text);
  }

  private void __initializeView(String text) {
    LayoutInflater layoutInflater = this.__getLayoutInflater(this.__context);
    layoutInflater.inflate(R.layout.view_portfolio_item, this, true);

    this.__textTv = (TextView) findViewById(R.id.view_portfolio_top_header__text);
    this.__textTv.setText(text);

    this.__formatView();
  }

  /**
   * Format view according to options.
   */
  private void __formatView() {
    // Format header.
    if (this.__cellType == CellType.HEADER) {
      setBackgroundResource(R.drawable.portfolio_symbol_header);
    }
    // Format footer.
    else if (this.__cellType == CellType.FOOTER) {
      this.__textTv.setTypeface(null, Typeface.BOLD);
      setBackgroundResource(R.drawable.portfolio_symbol_cell);

    }
    // Format data cell.
    else if (this.__cellType == CellType.DATA) {
      setBackgroundResource(R.drawable.portfolio_symbol_cell);
    }

    // Format indicators.
    if (this.__showIndicator) {
      double value = 0;
      try {
        value = Double.valueOf(this.__textTv.getText().toString());
      } catch (Exception e) { }
      if (value > 0) {
        this.__textTv.setTextColor(Color.GREEN);
      } else {
        this.__textTv.setTextColor(Color.RED);
      }
    }
  }

  /**
   * Return v4 layout inflater.
   * @param context
   * @return
   */
  private LayoutInflater __getLayoutInflater(Context context) {
    LayoutInflater result = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
      .cloneInContext(context);
    LayoutInflaterCompat.setFactory(result, new LayoutInflaterFactory() {
      @Override
      public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        return null;
      }
    });
    return result;
  }
}
