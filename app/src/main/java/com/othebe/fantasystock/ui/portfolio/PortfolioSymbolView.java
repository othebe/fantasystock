package com.othebe.fantasystock.ui.portfolio;

import android.content.Context;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v4.view.LayoutInflaterFactory;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.othebe.fantasystock.R;


/**
 * Created by otheb on 4/12/2016.
 */
public class PortfolioSymbolView extends RelativeLayout {
  private Context __context;
  private String __title;
  private String __subtitle;

  public PortfolioSymbolView(Context context, String title, String subtitle) {
    super(context);

    this.__context = context;
    this.__title = title;
    this.__subtitle = subtitle;

    this.__initialize();
  }

  public PortfolioSymbolView(Context context, String title) {
    super(context);

    this.__context = context;
    this.__title = title;

    this.__initialize();
  }

  private void __initialize() {
    // Inflate layout.
    LayoutInflater layoutInflater = this.__getLayoutInflater(this.__context);
    layoutInflater.inflate(R.layout.view_portfolio_symbol, this, true);

    // Populate elements.
    TextView titleTv = (TextView) findViewById(R.id.view_portfolio_table_item__title);
    titleTv.setText(this.__title);

    TextView subtitleTv = (TextView) findViewById(R.id.view_portfolio_table_item__subtitle);
    subtitleTv.setText(this.__subtitle);
    subtitleTv.setEllipsize(TextUtils.TruncateAt.END);
  }

  /**
   * Return v4 layout inflater.
   * @param context
   * @return
   */
  private LayoutInflater __getLayoutInflater(Context context) {
    LayoutInflater result = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
      .cloneInContext(context);
    LayoutInflaterCompat.setFactory(result, new LayoutInflaterFactory() {
      @Override
      public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        return null;
      }
    });
    return result;
  }
}
