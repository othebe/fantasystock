package com.othebe.fantasystock.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.othebe.fantasystock.MainActivity;
import com.othebe.fantasystock.R;
import com.othebe.fantasystock.constants.Constants;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This is the options screen.
 */
public class OptionsActivity extends BaseDrawerActivity {
  private ListView __optionsList;

  @Override
  int getLayoutId() {
    return R.layout.content_options;
  }

  @Override
  void onAfterCreate(Bundle savedInstanceState) {
    this.__initializeOptionsList();
  }

  private void __initializeOptionsList() {
    final String KEY_TITLE = "key_title";
    final String KEY_SUBTITLE = "key_subtitle";

    String[] from = { KEY_TITLE, KEY_SUBTITLE };
    int[] to = { R.id.item_option__title, R.id.item_option__subtitle };

    ArrayList<HashMap<String, String>> data = new ArrayList<>();

    // Reset portfolio.
    data.add(new HashMap<String, String>());
    data.get(data.size() - 1).put(KEY_TITLE, this.getString(R.string.option_reset_portfolio));
    data.get(data.size() - 1).put(KEY_SUBTITLE, this.getString(R.string.option_reset_portfolio_text));

    // Version.
    data.add(new HashMap<String, String>());
    data.get(data.size() - 1).put(KEY_TITLE, this.getString(R.string.option_version));
    data.get(data.size() - 1).put(KEY_SUBTITLE, "Dev 0.0");

    SimpleAdapter simpleAdapter = new SimpleAdapter(this, data, R.layout.item_option, from, to);

    this.__optionsList = (ListView) findViewById(R.id.options__listview);
    this.__optionsList.setAdapter(simpleAdapter);

    this.__initializeClickHandlers();
  }

  private void __initializeClickHandlers() {
    final OptionsActivity self = this;

    this.__optionsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d("OPTION", "POSITION - " + position);
        switch(position) {
          case 0:
            self.deleteDatabase(Constants.DB_NAME);
            startActivity(new Intent(self, MainActivity.class));
            break;
        }
      }
    });
  }
}
