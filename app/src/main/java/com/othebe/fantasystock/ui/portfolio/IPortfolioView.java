package com.othebe.fantasystock.ui.portfolio;

import com.othebe.fantasystock.models.PortfolioItem;

/**
 * Created by otheb on 4/17/2016.
 */
public interface IPortfolioView {
  void attachPortfolio(PortfolioItem[] portfolio);
  void showEmptyUi();
  void hideEmptyUi();
}
