package com.othebe.fantasystock.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.othebe.fantasystock.R;
import com.othebe.fantasystock.constants.TransactionType;
import com.othebe.fantasystock.models.Stock;
import com.othebe.fantasystock.ui.transaction.PositionFragment;
import com.othebe.fantasystock.ui.stockDetails.TickerFragment;

/**
 * Created by otheb on 4/1/2016.
 */
public class BuyActivity extends BaseDrawerActivity {
  private Stock __stock;
  private FragmentManager __fragmentManager;
  private TickerFragment __tickerFragment;

  @Override
  int getLayoutId() {
    return R.layout.content_buy;
  }

  @Override
  void onAfterCreate(Bundle savedInstanceState) {
    this.__fragmentManager = this.getSupportFragmentManager();
    Bundle bundle = getIntent().getExtras();
    this.__stock = bundle.getParcelable(Stock.PARCELABLE_KEY);

    this.__addTicker();
    this.__addPosition();
    this.__handlePreviewButton();
  }

  @Override
  protected void onPause() {
    super.onPause();
  }

  private void __handlePreviewButton() {
    final BuyActivity self = this;
    Button previewBtn = (Button) findViewById(R.id.buy__action_btn);
    previewBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        EditText unitsEt = (EditText) findViewById(R.id.buy__units);
        float units = Float.parseFloat(unitsEt.getText().toString());

        Intent i = new Intent(self, TransactionPreviewActivity.class);
        Bundle bundle = new Bundle();

        bundle.putParcelable(Stock.PARCELABLE_KEY, self.__stock);
        bundle.putString(TransactionPreviewActivity.PARCELABLE_TRANSACTION_TYPE_KEY, TransactionType.BUY.toString());
        bundle.putFloat(TransactionPreviewActivity.PARCELABLE_UNITS_KEY, units);
        i.putExtras(bundle);

        startActivity(i);
      }
    });
  }

  /**
   * Add ticker to layout.
   */
  private void __addTicker() {
    this.__tickerFragment = new TickerFragment();
    this.__tickerFragment.setStock(this.__stock);

    FragmentTransaction fragmentTransaction = this.__fragmentManager.beginTransaction();
    fragmentTransaction.add(R.id.buy__ticker_container, this.__tickerFragment);
    fragmentTransaction.commit();
  }

  /**
   * Add position to layout.
   */
  private void __addPosition() {
    PositionFragment positionFragment = new PositionFragment();

    FragmentTransaction fragmentTransaction = this.__fragmentManager.beginTransaction();
    fragmentTransaction.add(R.id.buy__position_container, positionFragment);
    fragmentTransaction.commit();
  }
}
