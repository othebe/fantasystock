package com.othebe.fantasystock.search;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by otheb on 3/12/2016.
 */
public class SearchSuggestionFetcher extends AsyncTask<String, Void, JSONObject> {
  @Override
  protected JSONObject doInBackground(String... params) {
    JSONObject response = null;

    if (params.length == 0) {
      return null;
    }

    String query = params[params.length - 1];
    if (query.length() == 0) {
      return null;
    }

    try {
      URL url = this.__buildRequestURL(query);
      HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
      InputStream in = new BufferedInputStream(httpURLConnection.getInputStream());
      response = this.__buildJSONResponse(in);
    } catch (java.net.MalformedURLException e) {
      return null;
    } catch (java.io.IOException e) {
      return null;
    }

    return response;
  }

  /**
   * Returns the request URL for autocompleting the stock symbol.
   * @param query Query to autocomplete.
   * @return
   */
  private URL __buildRequestURL(String query) throws java.net.MalformedURLException {
    String reqStr = "http://d.yimg.com/aq/autoc?query=" + query + "&region=US&lang=en-US";
    return new URL(reqStr);
  }

  /**
   * Build JSON Object from an input stream.
   * @param in
   * @return
   */
  private JSONObject __buildJSONResponse(InputStream in) {
    JSONObject jsonObject = null;

    try {
      jsonObject = new JSONObject(IOUtils.toString(in));
    } catch(IOException e) {
      return null;
    } catch(JSONException e) {
      return null;
    }

    return jsonObject;
  }
}
