package com.othebe.fantasystock.search;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.BaseColumns;
import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by otheb on 3/12/2016.
 */
public class SearchSuggestionProvider extends ContentProvider {
  public static String AUTHORITY = "com.othebe.fantasystock.search.SearchSuggestionProvider";

  @Override
  public boolean onCreate() {
    return true;
  }

  @Nullable
  @Override
  public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
    MatrixCursor cursor = null;

    if (selectionArgs.length == 0) {
      return null;
    }
    String queryString = selectionArgs[0];
    if (queryString.length() == 0) {
      return null;
    }

    AsyncTask<String, Void, JSONObject> suggestionFetcher = new SearchSuggestionFetcher().execute
      (queryString);

    try {
      JSONObject response = suggestionFetcher.get();
      cursor = this.__parseSuggestionJSON(response);
    } catch (java.lang.InterruptedException e) {
      return null;
    } catch (java.util.concurrent.ExecutionException e) {
      return null;
    } catch (JSONException e) {
      return null;
    }

    return cursor;
  }

  @Nullable
  @Override
  public String getType(Uri uri) {
    return "String";
  }

  @Nullable
  @Override
  public Uri insert(Uri uri, ContentValues values) {
    return null;
  }

  @Override
  public int delete(Uri uri, String selection, String[] selectionArgs) {
    return 0;
  }

  @Override
  public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
    return 0;
  }

  /**
   * Parse the response JSON object into a Cursor.
   * @param response
   * @return
   */
  private MatrixCursor __parseSuggestionJSON(JSONObject response) throws JSONException {
    if (response == null) {
      return null;
    }

    String[] columnNames = {
      BaseColumns._ID,
      SearchManager.SUGGEST_COLUMN_TEXT_1,
      SearchManager.SUGGEST_COLUMN_TEXT_2,
      SearchManager.SUGGEST_COLUMN_INTENT_DATA
    };
    MatrixCursor cursor = new MatrixCursor(columnNames);

    JSONObject resultSet = response.getJSONObject("ResultSet");
    JSONArray results = resultSet.getJSONArray("Result");

    for (int i = 0; i < results.length(); i++) {
      /**
       * Contains symbol, name, exch, type, exchDisp, typeDisp
       */
      JSONObject result = results.getJSONObject(i);
      String symbol = result.getString("symbol");
      String name = result.getString("name");

      Object[] columns = { i, symbol, name, symbol };
      cursor.addRow(columns);
    }

    return cursor;
  }

  /**
   * Determines if a stock suggestion is allowed.
   * @param suggestion
   * @return
   */
  private boolean __isSuggestionAllowed(JSONObject suggestion) {
    try {
      String stockType = suggestion.getString("type");
      // Disallow options.
      if (stockType.compareTo("O") == 0) {
        return false;
      }
    } catch (JSONException e) {
      return false;
    }

    return true;
  }
}
