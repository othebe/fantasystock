package com.othebe.fantasystock.search;

import com.othebe.fantasystock.constants.Exchange;
import com.othebe.fantasystock.constants.StockType;

/**
 * Created by otheb on 3/12/2016.
 */
public class SearchSuggestion {
  private String __symbol;
  private String __name;
  private Exchange __exchange;
  private StockType __stockType;
  private String __exchangeDisplay;
  private String __stockTypeDisplay;

  public SearchSuggestion(String symbol, String name, String exchange, String stockType, String
    exchangeDisplay, String stockTypeDisplay) {
    this.__symbol = symbol;
    this.__name = name;
    this.__exchange = this.__parseExchange(exchange);
    this.__stockType = this.__parseStockType(stockType);
    this.__exchangeDisplay = exchangeDisplay;
    this.__stockTypeDisplay = stockTypeDisplay;
  }

  public String getSymbol() {
    return this.__symbol;
  }

  public String getName() {
    return this.__name;
  }

  public String getExchangeDisplay() {
    return this.__exchangeDisplay;
  }

  public String getStockTypeDisplay() {
    return this.__stockTypeDisplay;
  }

  private Exchange __parseExchange(String exchange) {
    switch (exchange) {
      case "NYQ":
        return Exchange.NYQ;
      case "PCX":
        return Exchange.PCX;
      default:
        return Exchange.OTHER;
    }
  }

  private StockType __parseStockType(String stockType) {
    switch (stockType) {
      case "S":
        return StockType.STOCK;
      case "E":
        return StockType.EQUITY;
      default:
        return StockType.OTHER;
    }
  }
}
